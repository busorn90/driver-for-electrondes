#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <inc/hw_memmap.h>
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "driverlib/ssi.h"
#include "driverlib/gpio.h"
#include <driverlib/interrupt.h>
#include <driverlib/sysctl.h>
#include "driverlib/pin_map.h"
#include <driverlib/timer.h>
#include <inc/tm4c123gh6pm.h>
#include <driverlib/systick.h>
#include "driverlib/adc.h"
#include "driverlib/uart.h"
#include "driverlib/eeprom.h"
#include "driverlib/flash.h"
#include "utils/uartstdio.h"

#define FREQUENCY_INTERRUPT 1
#define NUM_SSI_DATA 8
#define CS1 GPIO_PIN_3
#define SHDWN GPIO_PIN_4
#define RS GPIO_PIN_6
#define CS2 GPIO_PIN_7

/*******************************************************************************************/
/*        																				   */
/*										   PROTOTYPE									   */
/*																						   */
/*******************************************************************************************/

void SendToFirstPotentiometer(uint16_t data);
void SendToSecondPotentiometer(uint16_t data);
//void PortFIntHandler(void);

volatile unsigned long FallingEdges = 0;
volatile uint32_t ui32Period;
int pulse_width = 2;

const uint16_t pui8DataTx[NUM_SSI_DATA] =
{0x0088, 0x01F8, 0x02F8, 0x0088, 0x0301, 0x011F, 0x001F, 0x0301};
unsigned char cState;

// represents a State of the FSM
struct State{
	unsigned char out;     // PB7-4 to right motor, PB3-0 to left
	unsigned short wait;   // in ms units
};

typedef const struct State StateType;
StateType Fsm[3] = {
		{0x02, 1}, // Positive_Pulse
		{0x08, 1}, // Negative_Pulse
		{0x00, 0}, // Zero_Pulse
};

void SysTick_Wait(uint32_t delay){
	NVIC_ST_RELOAD_R = delay-1;  // number of counts to wait
	NVIC_ST_CURRENT_R = 0;       // any value written to CURRENT clears
	while((NVIC_ST_CTRL_R&0x00010000)==0){ // wait for count flag
	}

}

void SysTick_Wait1ms(uint32_t delay){
	uint32_t i;
	for(i=0; i<delay; i++){
		SysTick_Wait(40000);  // wait 1ms
	}
}



void SysTick_Init(void)
{
	NVIC_ST_CTRL_R = 0;               // disable SysTick during setup
	NVIC_ST_CTRL_R = 0x00000005;      // enable SysTick with core clock
}

int main(void)
{
	uint32_t read;
	uint32_t ui32OutputValue;
	bool goFlag = false;
	uint16_t ui16DataA;
	uint16_t ui16DataB;
	volatile uint32_t ui32VoltageValue;
	//int i = 0;
	//char buffer[4];
	uint32_t pui32Read[2];
	uint32_t pui32Data[2];
	uint32_t returnCode;

	/*
	 * Set the clock @ 40 MHz
	 */
	SysCtlClockSet(SYSCTL_SYSDIV_5|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);

	/*
	 * Enable clock for the used peripheral:
	 * - PORTF
	 * - PORTA
	 * - SSI0
	 * - TIMER0
	 * - ADC0
	 * - UART0
	 * - EEPROM
	 */
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);


	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

	/*
	 * Set the output PINs for the PORTF
	 * - PIN1 = RED LED
	 * - PIN2 = BLUE LED
	 * - PIN3 = GREEN LED
	 */

	/*
	 * Set the input for SW1
	 */


	GPIO_PORTF_LOCK_R = 0x4C4F434B;   // 2) unlock PortF PF0
	GPIO_PORTF_CR_R |= 0x1F;           // allow changes to PF4-0
	GPIO_PORTF_AMSEL_R &= 0x00;        // 3) disable analog function
	GPIO_PORTF_PCTL_R &= 0x00000000;   // 4) GPIO clear bit PCTL
	GPIO_PORTF_DIR_R &= ~0x11;          // 5.1) PF4,PF0 input,
	GPIO_PORTF_DIR_R |= 0x0E;          // 5.2) PF3-PF2-PF1 output
	GPIO_PORTF_AFSEL_R &= 0x00;        // 6) no alternate function
	GPIO_PORTF_PUR_R |= 0x11;          // enable pullup resistors on PF4,PF0
	GPIO_PORTF_DEN_R |= 0x1F;          // 7) enable digital pins PF4-PF0


	ui32OutputValue = 5000;


	//
	// Register the port-level interrupt handler. This handler is the first
	// level interrupt handler for all the pin interrupts.
	//

	GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	//GPIOIntRegister(GPIO_PORTF_BASE, &PortFIntHandler);
	GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_FALLING_EDGE);
	GPIOIntEnable(GPIO_PORTF_BASE, GPIO_PIN_4);




	GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, CS1|CS2|SHDWN|RS);

	GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE,GPIO_PIN_1|GPIO_PIN_3);

	/*
	 * Configure the UART peripheral
	 */
	GPIOPinConfigure(GPIO_PA0_U0RX);
	GPIOPinConfigure(GPIO_PA1_U0TX);
	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);
	/*
	UARTConfigSetExpClk(UART0_BASE, SysCtlClockGet(), 115200,
	       (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
	 */
	UARTStdioConfig(0, 9600, 40000000);

	/*
	 * Configure the SSI peripheral
	 */
	GPIOPinConfigure(GPIO_PA2_SSI0CLK);
	GPIOPinConfigure(GPIO_PA5_SSI0TX);
	GPIOPinTypeSSI(GPIO_PORTA_BASE,GPIO_PIN_5|GPIO_PIN_2);

	/*
	 * Frame length = 10 bits
	 * Frequency = 10 MHz
	 * Master Mode
	 * SPO = 0
	 * SPH = 0
	 */
	SSIConfigSetExpClk(SSI0_BASE,SysCtlClockGet(),SSI_FRF_MOTO_MODE_0,SSI_MODE_MASTER,10000,16);////10
	/*
	 * Enable the SSI0
	 */
	SSIEnable(SSI0_BASE);
	/*
	 * Configure the TimerA0 in order to generate a periodic interrupt every seconds
	 */

	TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
	ui32Period = (SysCtlClockGet()/FREQUENCY_INTERRUPT);
	TimerLoadSet(TIMER0_BASE, TIMER_A, ui32Period -1);


	/*
	 * Enable the SysTick timer, used for generating delays
	 */
	SysTickEnable();
	do
	{
		returnCode = EEPROMInit();
	}while (returnCode != EEPROM_INIT_OK);
	//	EEPROMMassErase();

	EEPROMRead((uint32_t *)&pui32Read, 0x0, sizeof(pui32Read));
	ui32OutputValue = pui32Read[0];
	pulse_width = pui32Read[1];



	/*
	 * Enable interrupt for from TimerA
	 */

	IntEnable(INT_TIMER0A);
	TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	IntMasterEnable();
	UARTprintf("Press a button to Start with the stored values\n");
	UARTprintf("Enter the voltage value:\n");

	char serial_buffer[5];
	do{
		while(UARTCharsAvail(UART0_BASE))
		{
			UARTgets(serial_buffer,5);
			goFlag = true;
		}
		if((GPIO_PORTF_DATA_R&0x11) != 0x11)
			read = 5; // PF0 into read
	}while(read !=5 & (!goFlag));
	if(read == 5)
		SysTick_Wait1ms(500);
	else
	{
		ui32OutputValue = (uint32_t) atoi(serial_buffer);
		UARTprintf("Taken value:");
		UARTprintf(serial_buffer);
	}

	UARTprintf("\nEnter the width value:\n");


	if(read!=5)
	{
		char serial_buffer_char[4];
		//i= 0;
		//char data[3];
		do{
			/*
			while(UARTCharsAvail(UART0_BASE))
			{
				if((i%2) == 0)
				{
					data[i/2] = (char)(UARTCharGetNonBlocking(UART0_BASE) & 0x000000FF);
					UARTCharPutNonBlocking(UART0_BASE, data[i/2]);
				}
				i++;
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, GPIO_PIN_2); //blink LED
				SysTick_Wait1ms(1); //delay 1 msec
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, 0);
			}
			 */
			while(UARTCharsAvail(UART0_BASE))
			{
				UARTgets(serial_buffer_char,4);
				UARTprintf("Taken value:");
				UARTprintf(serial_buffer_char);
				UARTprintf("\nPress a button to Start\n");

			}
			read = GPIO_PORTF_DATA_R&0x11; // PF0 into read
		}while(read == 0x11);

		pulse_width = (uint32_t) atoi(serial_buffer_char);


		//pulse_width = (uint32_t)atoi(data);

		//EEProm
		pui32Data[0] = ui32OutputValue;
		pui32Data[1] = pulse_width;
		EEPROMProgram((uint32_t *)&pui32Data, 0x00000, sizeof(pui32Data));
		SysTick_Wait1ms(500);



	}
	UARTprintf("Pulses generator started\n");
	UARTprintf("Press a button to Stop\n");
	/*
	//EEProm
	pui32Data[0] = ui32OutputValue;
	pui32Data[1] = pulse_width;
	EEPROMProgram((uint32_t *)&pui32Data, 0x00000, sizeof(pui32Data));
	SysTick_Wait1ms(500);
	 */
	/*
	switch(ui32OutputValue)
	{
	case 1000:
		ui16DataA = 0xE7;
		ui16DataB = 0X146;
		break;

	case 1500:
		ui16DataA = 0xDC;
		ui16DataB = 0X164;
		break;

	case 2000:
		ui16DataA = 0xC6;
		ui16DataB = 0X148;
		break;

	case 2500:
		ui16DataA = 0x0C6;
		ui16DataB = 0x196;
		break;

	case 3000:
		ui16DataA = 0xB0;
		ui16DataB = 0X1A0;
		break;

	case 3500:
		ui16DataA = 0xC7;
		ui16DataB = 0X1D3;
		break;

	case 4000:
		ui16DataA = 0xC6;
		ui16DataB = 0X1F0;
		break;

	case 4500:
		ui16DataA = 0xB0;
		ui16DataB = 0X1F0;
		break;

	case 5000:
		ui16DataA = 0x84;
		ui16DataB = 0X1C8;
		break;

	case 5500:
		ui16DataA = 0x84;
		ui16DataB = 0X1DC;
		break;

	case 6000:
		ui16DataA = 0x84;
		ui16DataB = 0X1F0;
		break;

	case 6500:
		ui16DataA = 0x63;
		ui16DataB = 0X1C3;
		break;

	case 7000:
		ui16DataA = 0x63;
		ui16DataB = 0X1D2;
		break;

	case 7500:
		ui16DataA = 0x6E;
		ui16DataB = 0X1FA;
		break;

	case 8000:
		ui16DataA = 0x63;
		ui16DataB = 0X1F0;
		break;

	case 8500:
		ui16DataA = 0x63;
		ui16DataB = 0X1FF;
		break;

	case 9000:
		ui16DataA = 0x42;
		ui16DataB = 0X1B4;
		break;

	case 9500:
		ui16DataA = 0x42;
		ui16DataB = 0X1BE;
		break;

	case 9999:
		ui16DataA = 0x42;
		ui16DataB = 0X1C8;
		break;

	default:
		ui16DataA = 0x0C6;
		ui16DataB = 0x196;
		break;
	}
	*/
	/*
	 * Set up CS1 and CS2
	 */

	GPIOPinWrite(GPIO_PORTA_BASE,CS1,CS1);
	GPIOPinWrite(GPIO_PORTA_BASE,CS2,CS2);


	/*
	 * Reset the two potentiometers
	 */

	GPIOPinWrite(GPIO_PORTA_BASE,RS,0x00);
	SysTick_Wait1ms(10);
	GPIOPinWrite(GPIO_PORTA_BASE,RS,RS);
	GPIOPinWrite(GPIO_PORTA_BASE,SHDWN,SHDWN);


	/*
	 * Set the correct value for potentiometer
	 */
	//FIRST IC
	ui16DataA = 0x1376;
	SendToFirstPotentiometer(ui16DataA); //Set the first potentiometer in the first IC
	SysTick_Wait1ms(10);
	///////////////////
	/*
	 * SendToFirstPotentiometer(ui16DataB); //Set the second potentiometer in the first IC
	SysTick_Wait1ms(10);
	//SECOND IC
	SendToSecondPotentiometer(ui16DataA); //Set the first potentiometer in the first IC
	SysTick_Wait1ms(10);
	SendToSecondPotentiometer(ui16DataB); //Set the second potentiometer in the first IC
	 */



	/*
	 * Start Timer0 to count
	 */

	TimerEnable(TIMER0_BASE, TIMER_A);


	/*
	 * Interrupt for GPIO
	 */
	//GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_FALLING_EDGE);
	//GPIOIntEnable(GPIO_PORTF_BASE,GPIO_PIN_4);


	while(1)
	{

		read = GPIO_PORTF_DATA_R&0x11; // PF0 into read
		if(read != 0x11){
			if(FallingEdges%2)
			{
				TimerEnable(TIMER0_BASE, TIMER_A);
			}
			else
			{
				TimerDisable(TIMER0_BASE, TIMER_A);
			}
			FallingEdges++;
			SysTick_Wait1ms(500);
			//GPIOPinWrite(GPIO_PORTF_BASE,GPIO_PIN_3,GPIO_PIN_3);


		}
	}
}

void Timer0AIntHandler(void)
{
	// Clear the timer interrupt
	TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

	for(cState = 0; cState <3; cState++)
	{
		// output based on current state
		GPIO_PORTB_DATA_R = Fsm[cState].out;
		GPIO_PORTF_DATA_R = Fsm[cState].out;

		// wait for time according to state
		SysTick_Wait1ms(Fsm[cState].wait*pulse_width);
	}
}

void SendToFirstPotentiometer(uint16_t data)
{
	GPIOPinWrite(GPIO_PORTA_BASE,CS1,0);
	SSIDataPut(SSI0_BASE, data);
	while(SSIBusy(SSI0_BASE))
	{
	}
	GPIOPinWrite(GPIO_PORTA_BASE,CS1,0x08);
}

void SendToSecondPotentiometer(uint16_t data)
{
	GPIOPinWrite(GPIO_PORTA_BASE,CS2,0);
	SSIDataPut(SSI0_BASE, data);
	while(SSIBusy(SSI0_BASE))
	{
	}
	GPIOPinWrite(GPIO_PORTA_BASE,CS2,0x80);
}
void PortFIntHandler(void){
	if(FallingEdges%2)
				{
					TimerEnable(TIMER0_BASE, TIMER_A);
				}
				else
				{
					TimerDisable(TIMER0_BASE, TIMER_A);
				}
				FallingEdges++;
				SysTick_Wait1ms(500);
}
